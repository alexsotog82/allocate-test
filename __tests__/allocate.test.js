const allocate = require('../allocate')
const {salesOrders,purchaseOrders} = require('../resources/orders')

describe('test for alloc function', () => {
    const mockResponse = [
        { id: 'S3', expectedDate: '2020-01-04' },
        { id: 'S5', expectedDate: '2020-02-01' },
        { id: 'S1', expectedDate: '2020-02-20' },
        { id: 'S4', expectedDate: '2020-03-05' },
    ]

    test('should be instance of function', () => {
        expect(typeof allocate).toBe('function')
    });
    
    test('should return an array, and the length of this array must be greater than 0 ', () => {
        const result = allocate(salesOrders,purchaseOrders)
        expect(result.length).toBeGreaterThan(0)
        expect(result.length).toBe(4)
    });


    test('should be return an array for clients that have arrived', () => {
        const result = allocate(salesOrders,purchaseOrders)
        expect(result).toEqual(mockResponse)
    });
    
    test('should return an array and its first position must be an object ', () => {
        const mockObject = { id: 'S3',expectedDate: '2020-01-04' }
        const result = allocate(salesOrders,purchaseOrders)
        expect(result[0]).toStrictEqual(mockObject)
        expect(result[0]).toHaveProperty('id')
        expect(result[0]).toHaveProperty('expectedDate')
    });

    test('should return a string if sales orders are empty or not provide array', () => {
        const result = allocate(null,purchaseOrders)
        expect(result).toBe('Sorry, Sales orders are empty')
    });

    test('should return a string if Purchase orders are empty or not provide array', () => {
        const result = allocate(salesOrders,null)
        expect(result).toBe('Sorry, Purchase Orders are empty')
    });
    
})