const {purchaseOrders, salesOrders} =require( "./resources/orders")

const allocate = (salesOrders =[] , purchaseOrders=[] ) => {
    if(salesOrders?.length === 0 || salesOrders === null || salesOrders === undefined) {
        return 'Sorry, Sales orders are empty'
    }

    if (purchaseOrders?.length === 0 | purchaseOrders === null || purchaseOrders === undefined) {
        return 'Sorry, Purchase Orders are empty'
    }

    const salesOrderList = salesOrders.sort((a,b) => new Date(a.created) - new Date(b.created))
    const purchasesOrderList = purchaseOrders.sort((a,b) => new Date(a.receiving) - new Date(b.receiving))
    let accumulatedInventory = 0;
    let deliveryOrders = []
    let jumpPurchasesOrdersRequire = []

    for(let i = 0; i < purchasesOrderList.length; i++ ) {
        let jumpOrders = i + jumpPurchasesOrdersRequire.length

        if (purchasesOrderList[jumpOrders]?.quantity !== undefined) {
            accumulatedInventory = accumulatedInventory + purchasesOrderList[jumpOrders]?.quantity

            if(salesOrderList[i]?.quantity <= accumulatedInventory) {

                deliveryOrders.push({id: salesOrderList[i].id, expectedDate: purchasesOrderList[jumpOrders].receiving})
                accumulatedInventory = accumulatedInventory - salesOrderList[i].quantity

            } else {

                accumulatedInventory = accumulatedInventory + purchasesOrderList[i +1].quantity

                if(salesOrderList[i].quantity <= accumulatedInventory) {

                    deliveryOrders.push({id: salesOrderList[i].id, expectedDate: purchasesOrderList[i+1].receiving})
                    jumpPurchasesOrdersRequire.push(salesOrderList[i])
                    accumulatedInventory = accumulatedInventory - salesOrderList[i].quantity
                }
            }
        }
        

    }

   return deliveryOrders
}

module.exports = allocate